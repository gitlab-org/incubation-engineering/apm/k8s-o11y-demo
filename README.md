# Kubernetes Observability Demo

A Helm project to deploy the "de facto" observability (metrics, logs, traces) stack into Kubernetes.

**Not for production use** - just for experimentation and exploration.
- AuthZ/N is all disabled, anonymous users are admin.
- Services are exposed as NodePort for ease of use with minikube.

This is an umbrella chart, referencing various other Helm charts. Specifically:
- [Prometheus Operator (Includes Grafana & Alert Manager)](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)
- [Elasticsearch](https://github.com/elastic/helm-charts/tree/master/elasticsearch)
- [Kibana](https://github.com/elastic/helm-charts/tree/master/kibana)
- [Fluent Bit](https://github.com/fluent/helm-charts/tree/main/charts/fluent-bit)
- [Jaeger (Elastic as database)](https://github.com/jaegertracing/helm-charts/blob/main/charts/jaeger)

## Usage

See the `Makefile` for example commands.

By default we set UI services to use `NodePort`. That way you can use `minikube service list` to view the local address.

All UI services have authentication disabled.

## Requirements

See `.tool-version` for versioned tools in use. Use [asdf](http://asdf-vm.com/) to install and select tools automatically.

- A Kubernetes cluster to deploy into (e.g. via `minikube`).
- Helm to run deployment.

Optionally:
- `kubectl` to inspect the deployments/forward ports.
- `minikube` to create a cluster locally and test.

## TODO:

- Example app with full instrumentation and synthetic load

