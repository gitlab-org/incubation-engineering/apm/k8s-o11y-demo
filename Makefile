CLUSTER_NODES=3
HELM_RELEASE=o11y-demo
HELM_CHART=./o11y-demo

.PHONY: minikube-start
minikube-start:
	minikube start --nodes $(CLUSTER_NODES)

.PHONY: helm-install
helm-install:
	helm install $(HELM_RELEASE) $(HELM_CHART) --dependency-update

.PHONY: helm-upgrade
helm-upgrade:
	helm upgrade $(HELM_RELEASE) $(HELM_CHART)

.PHONY: helm-dep-update
helm-dep-update:
	helm dep update $(HELM_CHART)

.PHONY: helm-delete
helm-delete:
	helm delete $(HELM_RELEASE)

.PHONY: minikube-service-list
minikube-service-list:
	minikube service list | grep -v "No node port"